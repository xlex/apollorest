const bcryptjs = require('bcrypt'),
    jwt = require('jsonwebtoken'),
    passport  = require('passport'),
    models = require('../models')
const User = models.User;
module.exports = {
    create: (req,res) => async (req,res) => {
        let newUser = { 
            id_oauth,
            display_name,
            mail,
            password,
            gitlab_token,
            github_token,
            type_of_register
        } = req.body;
        newUser.type_of_register = 1;
        let passwordHash = bcrypt.hashSync(newUser.password, 10);
        newUser.password = passwordHash;
        User.create(newUser)
            .then(user => {
                res.json({user});
            })
            .catch(err => {
                res.status(500).json({ err });
            });
    },
    login: async (req, res) => { 
        let loginUser = {
            mail,
            password
        } = req.body;
        if (loginUser.mail && loginUser.password) {
            let user = await User.findOne({where:{mail: loginUser.mail,password: passwordHash}});
            if (!user) {
                res.status(401).json({ msg: 'Invalid credentials', user });
            }
            let passwordHash = bcryptjs.hashSync(loginUser.password,10);
            let payload = { id: user.id };
            let token = jwt.sign(payload, jwtOptions.secretOrKey);
            res.json({ msg: 'ok', token: token });
        }
    },
    thirdLogin: async (accessToken,refreshToken,profile,done) => {
        console.log("profile: %j",profile);
        let loginUser = await User.findOne({where: {id_oauth:profile.id}});
        if(loginUser){ //user already exist
            console.log('user already exist');
            done(null, profile);
        }else{
            let newUser = { 
                id_oauth: '',
                display_name: '',
                mail: '',
                password: '',
                gitlab_token: '',
                github_token: '',
                type_of_register: ''
            };
            newUser.id_oauth = profile.id;
            newUser.display_name = profile.displayName
            newUser.type_of_register = (profile.provider == 'google')? 2 : 3;
            User.create(newUser)
                .then(user => {
                    console.log('new user created');
                    done(null, profile);
                })
                .catch(err => {
                    console.log(err);
                });
        }
    },
    thirdLoginSuccess: async (req,res) => {
        //let payload = { id: user.id };
        //let token = jwt.sign(payload, jwtOptions.secretOrKey);
        res.json({ msg: 'ok'});
    },
    getAll: async (req,res) => {
        User.findAll().then(users => res.json(users));
    }
}