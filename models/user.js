module.exports = (sequelize, DataTypes) => {
    return sequelize.define('User', {
        id_oauth: DataTypes.STRING,
        display_name: DataTypes.STRING,
        mail: DataTypes.STRING,
        password: DataTypes.STRING,
        gitlab_token: DataTypes.STRING,
        github_token: DataTypes.STRING,
        type_of_register: DataTypes.INTEGER
        })
};