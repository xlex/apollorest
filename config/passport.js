const passport = require('passport'),
    passportJWT = require('passport-jwt'),
    GoogleStrategy = require('passport-google-oauth20').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    userController = require('../controllers/user');

let ExtractJwt = passportJWT.ExtractJwt;
let JwtStrategy = passportJWT.Strategy;
let jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = "Gatito123*";

let jwtStrategy = new JwtStrategy(jwtOptions, function(jwt_payload, next) {
    let user = getUser({ id: jwt_payload.id });
    if (user) {
        next(null, user);
    } else {
        next(null, false);
    }
});

let googleStrategy = new GoogleStrategy({
    clientID: "695709855165-eshspiu4uk1meagm7cu5t8vrvh3rfpe9.apps.googleusercontent.com",
    clientSecret: "KD2ziIfrEhOmoLnk60FIZ4ym",
    callbackURL: 'http://localhost:3000/auth/google/callback'
},
    userController.thirdLogin
);
let facebookStrategy = new FacebookStrategy({
    clientID: "690072065064282",
    clientSecret: "577506626d6e63e81758bb656812cb52",
    callbackURL: 'http://localhost:3000/auth/facebook/callback'
},
    userController.thirdLogin
);

passport.use(jwtStrategy);
passport.use(googleStrategy);
passport.use(facebookStrategy);
module.exports = passport;
