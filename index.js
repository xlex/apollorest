const express = require('express'),
        bodyParser = require('body-parser'),
        model = require('./models'),
        app = express();

passport = require('./config/passport.js');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser((user, done) => {
    done(null, user);
});
passport.deserializeUser((user, done) => {
    done(null, user);
});

model.sequelize
  .authenticate()
  .then(() => console.log('Connection has been established successfully.'))
  .catch(err => console.error('Unable to connect to the database:', err));
  
model.sequelize.sync()
    .then(() => console.log('Sync ready'))
    .catch(err => console.log('Sync fail: '+err));


app.get('/protected', passport.authenticate('jwt', { session: false }), function(req, res) {
    res.json({ msg: 'Congrats! You are seeing this because you are authorized'});
});
app.get('/',(req,res)=>{res.send("Ready")});
require('./routes/Auth.js')(app);
app.listen(3000,()=>{
    console.log('Servidor iniciado en el puerto 3000') 
});