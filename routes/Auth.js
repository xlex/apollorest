const passport = require('passport'),
        userController = require('../controllers/user.js');
module.exports = (app) => {
    app.post(
        'auth/signup',
        userController.create
    );
    app.post(
        'auth/login',
        userController.login
    );
    app.get(
        '/auth/facebook',
        passport.authenticate('facebook')
    );
    app.get(
        '/auth/facebook/callback',
        passport.authenticate('facebook'),
        userController.thirdLoginSuccess
    );
    app.get(
        '/auth/google',
        passport.authenticate('google',{scope: ['profile']})
    );
    app.get(
        '/auth/google/callback',
        passport.authenticate('google'),
        userController.thirdLoginSuccess
    );
    app.get(
        '/user',
        userController.getAll
    );
}